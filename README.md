# MobaXterm License Generator

Allows you to generate and activate a copy of [MobaXterm](https://mobaxterm.mobatek.net/).

# About

Developer holds no responsibility with what people decide to do with this app. It was developed strictly for demonstration purposes only.
Developed under the following conditions:

- Visual Studio 2022 (17.6.4)
- v4.8.0 .NET Framework
- C# language

# Usage

If you wish to simply use the keygen, head over to the [Releases](https://github.com/Aetherinox/MobaXtermKeygen/releases) section and download the latest binary as a `zip` or `rar`. The binary release only includes one file:

- `MobaXtermKG.exe`

# Build

Download the source files and launch the `.csproj` project file in Visual Studio.

If you decide to modify or re-build my code, you are to not re-distribute. Unlike a lot of keygens, my files are free of malware, and I do not want people taking advantage of a quick solution that you can dump your non-sense malware into and re-distribute.

If you're looking to do a quick credits swap and re-distribute just so you can make a name for yourself; I'd highly recommend you actually learn C# and make something yourself.

# Signed Releases

As of `v1.0.0.0` and onwards, binaries are GPG signed with the key `CB5C4C30CD0D4028`. You can find the key available on most keyservers:

- [pgp.mit.edu](https://pgp.mit.edu/)
- [keyserver.ubuntu.com](keyserver.ubuntu.com)
- [keys.openpgp.org](https://keys.openpgp.org)

Binaries are also signed with a certificate which has the thumbprint `70575bdfb02b3f1b45a37431bef9a8c9933a4ace`. If you downloaded this elsewhere on the internet and the binary is not signed with that certificate thumbprint; **IT IS NOT MINE**. You should delete it.

Don't modify these unless you know what you're doing, improperly configured, the Activation and Response will not generate into a valid serial key.

# Virus Scans

Unfortunately, virus scanners such as VirusTotal can report false positives. There's not much control I have over that. These websites will also attempt to detect keygens; sometimes you can get by it, and sometimes you can't.

The only option I'd have is to contact these websites and tell them that it's a false positive, but I'm writing a keygen; I highly doubt they're going to be happy with doing anything.

I scanned this keygen with Windows Defender and it reported that the files are clean. The other scan reports are listed below:

- [VirusTotal](https://www.virustotal.com/gui/file/b3c95fed6aef299753119ac80d16b1036a5b2516605bf76677bde85fd817d44b)
- [Jotti](https://virusscan.jotti.org/en-US/filescanjob/xa0wccihfs)
- [Dr. Web](https://online76.drweb.com/cache/?i=8c7659223b46f7653cbe87da244c8db3)
- [MetaDefender](https://metadefender.opswat.com/results/file/bzIzMDcxMlhKNzVxdGpHTTNvbkVXSWRvU3I/regular/overview)

# Previews

![Main Screen](https://i.imgur.com/GrQETPN.png)
![Program Unregistered](https://i.imgur.com/IwHfGsO.png)
![Program Registered](https://i.imgur.com/sSLXAIJ.png)
![Registered + White theme](https://i.imgur.com/F8NTfb5.png)